<?php require_once("config/database.php"); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Shadowmelt - Legion</title>
    <link href='https://fonts.googleapis.com/css?family=Droid+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="icon" type="image/x-icon" href="../../../images/icon.html" />
    <link rel="stylesheet" type="text/css" href="../../../css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="shortcut icon" type="image/ico" href="../../../images/favicon.ico"/>
<link href="//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.css" type="text/css" rel="stylesheet" />
<script src="//code.jquery.com/jquery-latest.js"></script>
<script src="//cdn.rawgit.com/noelboss/featherlight/1.7.13/release/featherlight.min.js" type="text/javascript" charset="utf-8"></script>
</head>
<body>
<header>
<div id="header-vid">
<video autoplay="true">
		    <source type="video/webm" src="../../../video/bg.webm">
  		</video>
</div>
<div id="nav">
<div class="container">
<div id="logo">
<a href="../../../"><img src="../../../logo.png"></a> <!-- Possible server logo --> 
</div>
<div class="right"><!-- menus -->
<div class="links">
<ul>
	<li><a href="/index">Home</a></li>
	<li><a href="/login" data-featherlight="iframe" data-featherlight-iframe-height="550" data-featherlight-iframe-width="720">Login</a></li>
	<li><a href="/register">Register</a></li>
	<li><a target="_blank" href="https://www.forum.shadowmelt.com">Forum</a></li>
	<li><a target="_blank" href="https://github.com/shadowmelt/bug-tracker/issues">Bug Tracker</a></li>
	<li><a href="/howtoconnect">How To Play</a></li>
	<!--<li><a target="_blank" href="https://stats.shadowmelt.com">Server Status</a></li>-->
</ul>
</div>
		</div>
	</div>
</div>

<div class="video-overlay">
	<div class="container">
		<div class="right">
<br>
<div class="info-box">
<h1>Welcome to <span>Shadowmelt</span>!</h1>
<p><br>The Tomb of Sargeras has been reopened, and the demons of the Burning Legion pour into our world. Their full, terrifying might is fixed on summoning the Dark Titan to Azeroth—and they’ve already located the key to his return.</p>
<br> 
<p>With the Alliance and Horde devastated, only you can take up Warcraft’s most legendary artifacts, scour the ancient Broken Isles for relics of the Titans, and challenge the Legion before Azeroth’s last hope dies.</p>
<br>
<p>Steel yourself, champion. Extinction is imminent.</p>	
</div>
	</div>
		</div>
			</div> 
				</header>