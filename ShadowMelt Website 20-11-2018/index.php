<?php
include_once("header.php");
?>
<div class="content">
<div class="container row">
<div id="left-content">
<img id="newsicon" src="images/news.png"><h2>Shadowmelt - <span>news</span></h2>

<?php
						
						
						$sql = "SELECT * FROM news ORDER BY id DESC";
						if ($result = $mysqli->query($sql)) {

						/* fetch associative array */
						while ($row = $result->fetch_assoc()) {
						  /*	printf ("%s (%s)\n", $row["Name"], $row["CountryCode"]); */
						  
						    echo '<div class="post">';
							echo '<h3 align="center" style="color: white;">'.$row["title"].'</h3>';
							echo '<p>'.$row["text"].'</p>';
							echo '<p style="font-size: 10px;" align="right">Posted by: <b><i style="color: #0099ff;">'.$row["postedby"].'</i></b> @<b style="color: white;">'.$row["date"].'</b></p>';
							echo '</div>';
							
						}

						/* free result set */
						$result->close();
						}
						
						?>
</p>
</div>

<?php include_once('right-content.php'); ?>

	</div>
		</div>
<?php include_once("footer.php"); ?>