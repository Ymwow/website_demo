<?php include("upheader.php"); ?>
<?php
// Define variables and initialize with empty values
$new_password = $confirm_password = "";
$new_password_err = $confirm_password_err = "";
 
// Processing form data when form is submitted
if($_SERVER["REQUEST_METHOD"] == "POST"){
	
	function encryptpw($user, $pass){
	$user = strtoupper($user);
	$pass = strtoupper($user);
	return sha1($user.':'.$pass);
	}
	
	$bnetpassword = strtoupper(bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($_SESSION["username"])).":".strtoupper($_POST['pass']))))))));
 
    // Validate new password
    if(empty($_POST["new_password"])){
        $new_password_err = '<span style="color: orange;">Please enter the new password.</span>';     
    } else{
        $new_password = $_POST["new_password"];
    }
    
    // Validate confirm password
    if(empty($_POST["confirm_password"])){
        $confirm_password_err = '<span style="color: orange;">Please confirm the password.</span>';
    } else{
        $confirm_password = $_POST["confirm_password"];
        if(empty($new_password_err) && ($new_password != $confirm_password)){
            $confirm_password_err = '<span style="color: orange;">Password did not match.</span>';
        }
    }
        
		
		
		
    // Check input errors before updating the database
    if(empty($new_password_err) && empty($confirm_password_err)){
        // Prepare an update statement
        $sql = "UPDATE battlenet_accounts SET sha_pass_hash = ? WHERE id = ?";
        
        if($stmt = mysqli_prepare($link, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "si", $param_password, $param_id);
            
            // Set parameters
            $param_password = strtoupper(bin2hex(strrev(hex2bin(strtoupper(hash("sha256",strtoupper(hash("sha256", strtoupper($_SESSION["username"])).":".strtoupper($_POST["new_password"]))))))));
            $param_id = $_SESSION["id"];
            
            // Attempt to execute the prepared statement
            mysqli_stmt_execute($stmt);
			   
        }
        
        // Close statement
        mysqli_stmt_close($stmt);
		
	}	
		$results = array();
		
												$getid = 'SELECT * FROM battlenet_accounts WHERE email = "'.$_SESSION["username"].'"'; 
												//SQL select query 
												
												 $getidresult = mysqli_query($dbh, $getid)or die(mysqli_error($dbh));
												 //execute SQL statement 
												 
													$getidrows = mysqli_num_rows($getidresult); 
													// get number of rows returned 
												
												if($getidrows){
												
													while($row = mysqli_fetch_array($getidresult)){
														$results["id"] = $row;
																	
													}
												}
														$id = $results["id"]["id"];
														$bnetacc = $id;
														$username = $bnetacc."#".$bnetindex;
														$accpassword = encryptpw($username, $_POST["new_password"]);
		
	
	if(empty($new_password_err) && empty($confirm_password_err)){
        // Prepare an update statement
        $sql_acc = "UPDATE account SET sha_pass_hash = ? WHERE email = ?";
        
        if($stmt_acc = mysqli_prepare($link, $sql_acc)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt_acc, "ss", $param_password, $param_user);
            
            // Set parameters
            $param_password = $accpassword;
            $param_user = $_SESSION["username"];
            
            // Attempt to execute the prepared statement
            if(mysqli_stmt_execute($stmt_acc)){
                // Password updated successfully. Destroy the session, and redirect to login page
				
                echo '<script type="text/javascript">
				window.location = "logout"
					</script>';
				
            } else{
                echo '<span style="color: orange;">Oops! Something went wrong. Please try again later.</span>';
            }
        }
        
        // Close statement
        mysqli_stmt_close($stmt_acc);
		
    }
    
    // Close connection
    mysqli_close($link);
}
?>
<div class="content">
<div class="container row">

<div id="left-content">
<img id="newsicon" src="images/news.png"><h2>Shadowmelt - <span>User Control Panel</span> - <span>Password Change</span></h2> <h3 align="right">Hello <span style="color: yellow;">
<?php echo strtolower($_SESSION["username"]); ?></span>, 
<?php
$Hour = date("G");

if ( $Hour >= 8 && $Hour < 12 ) {
    echo "Good Morning";
} else if ( $Hour >= 12 && $Hour < 19 ) {
    echo "Good Afternoon";
} else if ( $Hour >= 19 || $Hour < 2 ) {
    echo "Good Evening";
} else if ( $Hour >= 2 || $Hour < 6 ) {
    echo "Good night! Still awake?!";
} else if ( $Hour >= 6 || $Hour < 8 ) {
    echo "Good night! Still awake?!";
}
?>
 <a style="color: orange;" href="/logout">LOGOUT</a> </h3>
 
 <div class="post"><h3 align="center" style="color: white;">Change Your Password</h3></div>

									<form action="password" method="post" name="reg">
				                        <table style="border-spacing: 30px; border-collapse: separate;" class="form">
				                            <tr>
				                                <td align="right">
				                                    New Password:
				                                </td>
				                                <td align="left">
												<div class="form-group <?php echo (!empty($new_password_err)) ? 'has-error' : ''; ?>">
				                                    <input style="width: 400px; font-size: 16px;" name="new_password" type="password" maxlength="16" value="<?php echo $new_password; ?>" placeholder="type the new password you want" required /><br>
													<span class="help-block"><?php echo $new_password_err; ?></span>
												</div>
				                                </td>
				                            </tr>
				                            <tr>
				                                <td align="right">
				                                    Confirm Password:
				                                </td>
				                                <td align="left">
												<div class="form-group <?php echo (!empty($confirm_password_err)) ? 'has-error' : ''; ?>">
				                                    <input style="width: 400px; font-size: 16px;" name="confirm_password" type="password" maxlength="16" placeholder="confirm new password again" required /><br>
													<span class="help-block"><?php echo $confirm_password_err; ?></span>
												</div>
				                                </td>
				                            </tr>
				                            <tr>              
												<td></td>
				                                <td align="center">
				                                    <input style="width: 100px;" type="submit" class="sbm" value="Change" name='submit' />
				                                </td>
				                            </tr>
				                        </table>
				                    </form>
									<div style="width: 300px; margin: auto;"><form action="userpanel"><button class="upmenu">BACK</button></form></div>
									<!-- <p align="center"><b>&sup1;</b> <i style="color: yellow;">Is required to use a valid email, to activate your Account, 
									since our system send you email with details on how to activate your account.</i></p> -->

</div>
<?php include_once("right-content.php"); ?>
	</div>
		</div>
<?php mysqli_close($dbh); ?>
<?php include_once("footer.php"); ?>